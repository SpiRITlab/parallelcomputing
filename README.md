# ParallelComputing

Run ```mvn package``` inside the folder of each module to compile and generate a .jar file.


If you need to sync your forked repo with upstream repo, check [here](https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/syncing-a-fork)

## Sync up to this repo

Add `upstream` repo URL to your forked repo
```bash
git remote add upstream https://gitlab.com/SpiRITlab/parallelcomputing.git
```

Then if you type ```git remote -v``` if should show the following
```bash
origin	https://gitlab.com/YOUR-USERNAME/parallelcomputing.git (fetch)
origin	https://gitlab.com/YOUR-USERNAME/parallelcomputing.git (push)
upstream	https://gitlab.com/SpiRITlab/parallelcomputing.git (fetch)
upsream		https://gitlab.com/SpiRITlab/parallelcomputing.git (push)
```

Now, run this command to pull from this repo
```bash
git pull upstream master
```

Once, the pull command is done. Your repo should have my latest changes. Then push the changes to your forked repo.
```bash
git push
```

Note, you might need to resolve conflicts if there are mismatches in the two repo.


## Docker: creating image and container
Please run ```docker images``` and check whether ```csci654``` image exist. 

If not, run the following to create a docker image. 
```
docker build -t csci654:latest .
```
Verify the new image with 
```
docker images
```

Start a container
```
docker run -it csci654 /bin/bash
```

## Docker-compose: creating multiple docker containers at once
Build and start docker containers
```
docker-compose up
```
use `--build` to rebuild docker image

Attach to the running docker containers
```
docker exec -it <CONTAINER-NAME> bash
```
where `<CONTAINER-NAME>` should be replaced with your targeted container name.

