package edu.rit.cs;

import mpi.MPI;
import mpi.MPIException;

public class MPI_Send_Recv {
    public static void main(String args[]) throws MPIException {
        MPI.Init(args);

        int rank = MPI.COMM_WORLD.getRank(), size = MPI.COMM_WORLD.getSize();

        String hostname = MPI.getProcessorName();
        System.out.println("From Java Program: Number of tasks= "+size+", My rank="+rank+", Running on "+hostname);

        int src, dst;
        int num=9;
        // task 0 sends to task 1 and waits to receive a return message
        if (rank == 0) {
            dst = 1;
            src = 1;
            MPI.COMM_WORLD.send(num,1,MPI.INT,dst,0);
            MPI.COMM_WORLD.recv(num,1,MPI.INT,src,0);
        }
        // task 1 waits for task 0 message then returns a message
        else if (rank != 0){
            dst = 0;
            src = 0;
            MPI.COMM_WORLD.recv(num,1,MPI.INT,src,0);
            MPI.COMM_WORLD.send(num,1,MPI.INT,dst,0);
        }

        MPI.Finalize();
    }
}
