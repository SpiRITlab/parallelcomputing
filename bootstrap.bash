#!/bin/bash

CURRENT_DIR=$(pwd)

DEPS_ROOT=$CURRENT_DIR/dependencies
JARS_DIR=$DEPS_ROOT/jars

openmp_version=1.2
openmpi_version_major=4.0
openmpi_version_minor=1
openmpi_version=$(openmpi_version_major).$(openmpi_version_minor)

mkdir -p $DEPS_ROOT $JARS_DIR

cd $JARS_DIR
wget https://github.com/omp4j/omp4j/releases/download/v$(openmp_version)/omp4j-$(openmp_version).jar

cd $DEPS_ROOT
wget https://download.open-mpi.org/release/open-mpi/v$(openmpi_version_major)/openmpi-$(openmpi_version).tar.gz && \
    tar xfzv openmpi-$(openmpi_version).tar.gz && \
    rm -rf openmpi-$(openmpi_version).tar.gz && \
    cd openmpi-$(openmpi_version) && \
    ./configure --enable-mpi-java && \
    make && make install && \
    rm -rf ../openmpi-$(openmpi_version).tar.gz && \
    cp ompi/mpi/java/java/mpi.jar $JARS_DIR/ && \
    echo "Run 'export LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:/usr/local/lib >> /etc/profile' to add library into your path."

cd $CURRENT_DIR