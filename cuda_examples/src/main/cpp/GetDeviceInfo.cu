// For infor about these example, see the links below
// https://devblogs.nvidia.com/how-query-device-properties-and-handle-errors-cuda-cc/
// https://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__DEVICE.html#group__CUDART__DEVICE_1g1bf9d625a931d657e08db2b4391170f0
// https://docs.nvidia.com/cuda/cuda-runtime-api/structcudaDeviceProp.html#structcudaDeviceProp_1dee14230e417cb3059d697d6804da414
#include <iostream>

// Host code
int main(void)
{
    int nDevices;
    cudaError_t err = cudaGetDeviceCount(&nDevices);
    if (err != cudaSuccess) printf("%s\n", cudaGetErrorString(err));

    for (int i = 0; i < nDevices; i++) {
        cudaDeviceProp prop;
        cudaGetDeviceProperties(&prop, i);
        printf("Device Number: %d\n", i);

        // ASCII string identifying device
        printf("  Device name: %s\n", prop.name);

        // Maximum number of threads per block
        printf("  Max Threads Per Block: %d\n", prop.maxThreadsPerBlock);

        // Maximum size of each dimension of a block
        printf("  Max Threads Dim: x:%d, y:%d, z:%d\n", prop.maxThreadsDim[0], prop.maxThreadsDim[1], prop.maxThreadsDim[2]);

        // Maximum size of each dimension of a grid
        printf("  Max Grid Size: x:%d, y:%d, z:%d\n", prop.maxGridSize[0], prop.maxGridSize[1], prop.maxGridSize[2]);

        // Clock frequency in kilohertz
        printf("  Clock Rate (KHz): %d\n", prop.clockRate);

        // Peak memory clock frequency in kilohertz
        printf("  Memory Clock Rate (KHz): %d\n", prop.memoryClockRate);

        // Global memory bus width in bits
        printf("  Memory Bus Width (bits): %d\n", prop.memoryBusWidth);

        printf("  Peak Memory Bandwidth (GB/s): %f\n\n", 2.0*prop.memoryClockRate*(prop.memoryBusWidth/8)/1.0e6);
    }

}