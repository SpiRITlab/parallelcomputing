# Mining Pool

## Compile
1. Login to any of the docker cluster machine (e.g., hydr00) provided by the CS department.
2. Go into the `mining_pool` folder of your git cloned repo.
3. Execute the command `mvn package` which produces jar files in the `target` folder.


## Running the Miner and analysis
Login to your assigned CS lab machine to execute the following commands. Only run the following commands on the assigned CS lab machine.
### Launch the Miner
```java -cp target/mining_pool-1.0-SNAPSHOT.jar edu.rit.cs.mining.miner.Miner <registry hostname>```
Lookup the registry hostname in the course assignment page on myCourses.
Note, to quit the mining code, press `q` then press `enter`/`return`.

If your process is stuck, then you can open another terminal and ssh into this machine and manually kill the staled process.
1. Run `ps ax`; then you may see this, `25791 pts/1    Sl+    0:00 java -cp target/mining_pool-1.0-SNAPSHOT.jar edu.rit.cs.mining.miner.Miner <registry hostname>`
2. Then, run `kill -9 <PID>`; e.g., `kill -9 25791` for my example above.

### Extract Block Info
```java -cp target/mining_pool-1.0-SNAPSHOT.jar edu.rit.cs.mining.ExtractBlockInfo <path>/Ledger.dat```

### Generate Summary
```java -cp target/mining_pool-1.0-SNAPSHOT.jar edu.rit.cs.mining.GenerateSummary <path>/Ledger.dat```


## Running the Registry (Don't need for miner)
```java -cp target/mining_pool-1.0-SNAPSHOT.jar edu.rit.cs.mining.registry.Registry```
